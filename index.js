const http = require("http");
const port = 4000;

http
  .createServer((req, res) => {
    // If the url is http://localhost:4000/, send a response Welcome to Booking System
    if (req.url == "/" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "plain/text" });
      res.end("Welcome to booking system");
    }

    // If the url is http://localhost:4000/profile, send a response Welcome to your profile!
    if (req.url == "/profile" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "plain/text" });
      res.end("Welcome to your profile!");
    }

    // If the url is http://localhost:4000/courses, send a response Here’s our courses available.
    if (req.url == "/courses" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "plain/text" });
      res.end("Here’s our courses available.");
    }

    // If the url is http://localhost:4000/addcourse, send a response Add a course to our resources.
    if (req.url == "/addcourse" && req.method == "POST") {
      res.writeHead(200, { "Content-Type": "plain/text" });
      res.end("Add a course to our resources.");
    }

    // If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources.
    if (req.url == "/updatecourse" && req.method == "PUT") {
      res.writeHead(200, { "Content-Type": "plain/text" });
      res.end("Update a course to our resources.");
    }

    // If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources.
    if (req.url == "/archivecourses" && req.method == "DELETE") {
      res.writeHead(200, { "Content-Type": "plain/text" });
      res.end("Archive courses to our resources.");
    }
  })
  .listen(port, "localhost", () => {
    console.log(`"EDIT: System is now running at localhost ${port}`);
  });
